-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 02, 2020 at 11:51 PM
-- Server version: 5.7.18
-- PHP Version: 7.4.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `jobs_test`
--

-- --------------------------------------------------------

--
-- Table structure for table `companies`
--

CREATE TABLE `companies` (
  `id` smallint(5) UNSIGNED NOT NULL,
  `name` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `companies`
--

INSERT INTO `companies` (`id`, `name`) VALUES
(1, 'Company A'),
(2, 'Company B'),
(3, 'Company C'),
(4, 'Company D'),
(5, 'Company E'),
(6, 'Company F'),
(7, 'Company G'),
(8, 'Company H'),
(9, 'Company J'),
(10, 'Company K');

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--

CREATE TABLE `jobs` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `name` varchar(32) NOT NULL,
  `company_id` smallint(5) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jobs`
--

INSERT INTO `jobs` (`id`, `name`, `company_id`) VALUES
(1, 'Job A', 1),
(2, 'Job B', 2),
(3, 'Job C', 3),
(4, 'Job D', 4),
(5, 'Job E', 5),
(6, 'Job F', 6),
(7, 'Job G', 7),
(8, 'Job H', 8),
(9, 'Job J', 9),
(10, 'Job K', 10);

-- --------------------------------------------------------

--
-- Table structure for table `jobs_requirements`
--

CREATE TABLE `jobs_requirements` (
  `job_id` mediumint(8) UNSIGNED NOT NULL,
  `requirement_id` smallint(5) UNSIGNED NOT NULL,
  `group_id` tinyint(3) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jobs_requirements`
--

INSERT INTO `jobs_requirements` (`job_id`, `requirement_id`, `group_id`) VALUES
(1, 1, 1),
(1, 2, 1),
(1, 3, 0),
(2, 4, 1),
(2, 5, 1),
(2, 6, 0),
(2, 7, 0),
(3, 8, 0),
(3, 9, 0),
(4, 1, 1),
(4, 2, 1),
(4, 10, 1),
(5, 4, 1),
(5, 5, 1),
(5, 6, 0),
(5, 11, 1),
(5, 12, 1),
(6, 6, 0),
(6, 13, 1),
(6, 14, 1),
(6, 15, 1),
(6, 16, 0),
(7, 17, 0),
(7, 18, 0),
(8, 19, 1),
(8, 20, 1),
(10, 21, 0);

-- --------------------------------------------------------

--
-- Table structure for table `requirements`
--

CREATE TABLE `requirements` (
  `id` smallint(5) UNSIGNED NOT NULL,
  `name` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `requirements`
--

INSERT INTO `requirements` (`id`, `name`) VALUES
(11, '2 door car'),
(12, '3 door car'),
(5, '4 door car'),
(4, '5 door car'),
(1, 'apartment'),
(14, 'bike'),
(7, 'car insurance'),
(6, 'driver\'s license'),
(10, 'flat'),
(20, 'garage'),
(2, 'house'),
(18, 'liability insurance'),
(17, 'massage qualification certificate'),
(15, 'motorcycle'),
(16, 'motorcycle insurance'),
(21, 'PayPal account'),
(3, 'property insurance'),
(13, 'scooter'),
(8, 'social security number'),
(19, 'storage place'),
(9, 'work permit');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `companies`
--
ALTER TABLE `companies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jobs`
--
ALTER TABLE `jobs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `company_id` (`company_id`);

--
-- Indexes for table `jobs_requirements`
--
ALTER TABLE `jobs_requirements`
  ADD UNIQUE KEY `job_requirement` (`job_id`,`requirement_id`),
  ADD KEY `requirement_id` (`requirement_id`);

--
-- Indexes for table `requirements`
--
ALTER TABLE `requirements`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `companies`
--
ALTER TABLE `companies`
  MODIFY `id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `jobs`
--
ALTER TABLE `jobs`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `requirements`
--
ALTER TABLE `requirements`
  MODIFY `id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `jobs`
--
ALTER TABLE `jobs`
  ADD CONSTRAINT `jobs_ibfk_1` FOREIGN KEY (`company_id`) REFERENCES `companies` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `jobs_requirements`
--
ALTER TABLE `jobs_requirements`
  ADD CONSTRAINT `jobs_requirements_ibfk_1` FOREIGN KEY (`job_id`) REFERENCES `jobs` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `jobs_requirements_ibfk_2` FOREIGN KEY (`requirement_id`) REFERENCES `requirements` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
