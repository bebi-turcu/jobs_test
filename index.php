<h3>Trying to match jobs with assets:</h3>
<pre>
<?php
$assets = array('bike', 'driver\'s license');
//$assets = array('4 door car', 'driver\'s license');
//$assets = array('house', 'property insurance');
print_r($assets);

$dbName = 'jobs_test';
$dbUser = 'root';
$dbPass = '';
$pdo = new PDO('mysql:host=localhost;dbname='. $dbName, $dbUser, $dbPass);

$results = $pdo->query('
    SELECT jr.job_id, j.name AS job, c.name AS company, r.name AS requirement, jr.group_id
    FROM jobs_requirements jr
    INNER JOIN jobs j ON j.id = jr.job_id
    INNER JOIN companies c ON c.id = j.company_id
    INNER JOIN requirements r ON r.id = jr.requirement_id
    WHERE jr.job_id IN(
        SELECT DISTINCT job_id FROM jobs_requirements WHERE requirement_id IN(
            SELECT id FROM requirements WHERE name IN("'. implode('", "', $assets) .'")
        )
    )
    UNION ALL
    SELECT j.id, j.name AS job, c.name AS company, NULL AS requirement, 0 AS group_id
    FROM jobs j
    INNER JOIN companies c ON c.id = j.company_id
    LEFT JOIN jobs_requirements jr ON jr.job_id = j.id
    WHERE jr.job_id IS NULL
');

$jobs = array();
while ($row = $results->fetch(PDO::FETCH_ASSOC)) {
    $id = (int) $row['job_id'];
    if (!isset($jobs[$id])) {
        $jobs[$id] = array(
            'name'         => $row['job'],
            'company'      => $row['company'],
            'requirements' => array()
        );
    }
    if ($row['requirement']) {
        $jobs[$id]['requirements'][(int) $row['group_id']][] = $row['requirement'];
    }
}

echo '<h3>Jobs accepting at least one of the assets above:<br />
(requirements in group_id = 0 are all mandatory, while group_id >= 1 holds multiple options)</h3>';
print_r($jobs);
echo '</pre>';

foreach ($jobs as $id => $job) {
    unset($jobs[$id]);
    foreach ($job['requirements'] as $groupId => $group) {
        if ($groupId) { # multiple options, check if at least one is found in $assets
            foreach ($group as $requirement) {
                if (in_array($requirement, $assets)) {
                    continue 2; # jump to next group
                }
            }
            continue 2; # not found in $assets, so jump to next job
        } else {    # all requirements here are mandatory
            foreach ($group as $requirement) {
                if (!in_array($requirement, $assets)) {
                    continue 3; # not found in $assets, so jump to next job
                }
            }
        }
    }
    # if all goes well, replace the job data with a message
    $jobs[$id] = "Found match with job_id={$id}, {$job['name']} at {$job['company']}";
}

echo '<h3>Matched jobs:</h3>';
if ($jobs) {
    echo implode('<br />', $jobs);
} else {
    echo 'No jobs found';
}